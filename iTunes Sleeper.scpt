FasdUAS 1.101.10   ��   ��    k             l     ��  ��    C = Ask user for the time (in minutes) to let the music fade out     � 	 	 z   A s k   u s e r   f o r   t h e   t i m e   ( i n   m i n u t e s )   t o   l e t   t h e   m u s i c   f a d e   o u t   
  
 l     ����  I    ��  
�� .gtqpchltns    @   @ ns    J     
       m        �    1 8 0      m       �    1 2 0      m       �    9 0      m       �    4 5     !   m     " " � # #  3 0 !  $ % $ m     & & � ' '  1 5 %  ( ) ( m     * * � + +  5 )  ,�� , m     - - � . .  1��    �� / 0
�� 
appr / m     1 1 � 2 2 $ i T u n e s   S l e e p   T i m e r 0 �� 3 4
�� 
prmp 3 m     5 5 � 6 6 x S e t   t h e   t i m e   ( i n   m i n u t e s )   y o u   w o u l d   l i k e   i T u n e s   t o   f a d e   o u t . 4 �� 7��
�� 
inSL 7 m     8 8 � 9 9  9 0��  ��  ��     : ; : l     ��������  ��  ��   ;  < = < l     �� > ?��   >   Get the fade out time    ? � @ @ ,   G e t   t h e   f a d e   o u t   t i m e =  A B A l     C���� C r      D E D c     F G F 1    ��
�� 
rslt G m    ��
�� 
nmbr E o      ���� 0 fadetime fadeTime��  ��   B  H I H l     �� J K��   J ' ! Convert the fade time to seconds    K � L L B   C o n v e r t   t h e   f a d e   t i m e   t o   s e c o n d s I  M N M l  ! , O���� O r   ! , P Q P ]   ! ( R S R o   ! $���� 0 fadetime fadeTime S m   $ '���� < Q o      ���� 0 fadetime fadeTime��  ��   N  T U T l     ��������  ��  ��   U  V W V l     �� X Y��   X   Go to sleep.    Y � Z Z    G o   t o   s l e e p . W  [�� [ l  - � \���� \ O   - � ] ^ ] Z   3 � _ `���� _ =  3 < a b a 1   3 8��
�� 
pPlS b m   8 ;��
�� ePlSkPSP ` k   ? � c c  d e d r   ? H f g f 1   ? D��
�� 
pVol g o      ���� 0 	maxvolume 	maxVolume e  h i h l  I I�� j k��   j 1 + Divide by maxVolume to get delay increment    k � l l V   D i v i d e   b y   m a x V o l u m e   t o   g e t   d e l a y   i n c r e m e n t i  m n m r   I T o p o ^   I P q r q o   I L���� 0 fadetime fadeTime r o   L O���� 0 	maxvolume 	maxVolume p o      ���� 0 	delaytime 	delayTime n  s t s r   U x u v u b   U t w x w b   U p y z y b   U h { | { b   U d } ~ } b   U `  �  b   U \ � � � m   U X � � � � �  S t a r t i n g   a t   � o   X [���� 0 	maxvolume 	maxVolume � m   \ _ � � � � � H % ,   t h e   m u s i c   w i l l   f a d e   o u t   1 %   e v e r y   ~ o   ` c���� 0 	delaytime 	delayTime | m   d g � � � � � 2   s e c o n d s ,   f o r   a   t o t a l   o f   z ^   h o � � � o   h k���� 0 fadetime fadeTime � m   k n���� < x m   p s � � � � � >   m i n u t e s   b e f o r e   p a u s i n g   i T u n e s . v o      ���� 0 	delayinfo 	delayInfo t  � � � I  y ��� ���
�� .ascrcmnt****      � **** � o   y |���� 0 	delayinfo 	delayInfo��   �  ��� � T   � � � � k   � � � �  � � � r   � � � � � 1   � ���
�� 
pVol � o      ���� 0 currentvolume currentVolume �  ��� � Z   � � � ��� � � l  � � ����� � ?   � � � � � o   � ����� 0 currentvolume currentVolume � m   � �����  ��  ��   � k   � � � �  � � � I  � ��� ���
�� .sysodelanull��� ��� nmbr � o   � ����� 0 	delaytime 	delayTime��   �  ��� � r   � � � � � \   � � � � � o   � ����� 0 currentvolume currentVolume � m   � �����  � 1   � ���
�� 
pVol��  ��   � k   � � � �  � � � I  � �������
�� .hookPausnull        null��  ��   �  � � � l  � ��� � ���   � #  Reset original iTunes volume    � � � � :   R e s e t   o r i g i n a l   i T u n e s   v o l u m e �  � � � r   � � � � � o   � ����� 0 	maxvolume 	maxVolume � 1   � ���
�� 
pVol �  ��� �  S   � ���  ��  ��  ��  ��   ^ m   - 0 � ��                                                                                  hook  alis    8  DRIVE                      �ymH+  4~�
iTunes.app                                                     8�t�,��        ����  	                Applications    ���      �,�
    4~�  DRIVE:Applications: iTunes.app   
 i T u n e s . a p p    D R I V E  Applications/iTunes.app   / ��  ��  ��  ��       �� � ���   � ��
�� .aevtoappnull  �   � **** � �� ����� � ���
�� .aevtoappnull  �   � **** � k     � � �  
 � �  A � �  M � �  [����  ��  ��   �   � $     " & * -���� 1�� 5�� 8������������ ����������� � � � ������������� 
�� 
appr
�� 
prmp
�� 
inSL�� 
�� .gtqpchltns    @   @ ns  
�� 
rslt
�� 
nmbr�� 0 fadetime fadeTime�� <
�� 
pPlS
�� ePlSkPSP
�� 
pVol�� 0 	maxvolume 	maxVolume�� 0 	delaytime 	delayTime�� 0 	delayinfo 	delayInfo
�� .ascrcmnt****      � ****�� 0 currentvolume currentVolume
�� .sysodelanull��� ��� nmbr
�� .hookPausnull        null�� ����������v������� O_ a &E` O_ a  E` Oa  �*a ,a   �*a ,E` O_ _ !E` Oa _ %a %_ %a %_ a !%a %E` O_ j  O BhZ*a ,E` !O_ !j _ j "O_ !k*a ,FY *j #O_ *a ,FO[OY��Y hUascr  ��ޭ