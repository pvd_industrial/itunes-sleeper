# Ask user for the amount of time to let the music fade out
choose from list {"240", "180", "120", "90", "45", "30"} with title "iTunes Sleep Timer" with prompt "Set the time (in minutes) you would like iTunes to fade out." default items "120"

# Get the fade out time
set fadeTime to result as number
# Convert the fade time to seconds
set fadeTime to fadeTime * 60

# Go to sleep.
tell application "iTunes"
	if player state is playing then
		set maxVolume to sound volume
		# Divide by maxVolume to get delay increment
		set delayTime to fadeTime / maxVolume
		set delayInfo to "Starting at " & maxVolume & "%, the music will fade out 1% every " & delayTime & " seconds, for a total of " & fadeTime / 60 & " minutes before pausing iTunes."
		log delayInfo
		repeat
			set currentVolume to sound volume
			if (currentVolume > 0) then
				delay delayTime
				set sound volume to currentVolume - 1
			else
				pause
				# Reset original iTunes volume
				set sound volume to maxVolume
				exit repeat
			end if
		end repeat
	end if
end tell